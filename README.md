# Docker container for interaction with Ansible/AWS #

![Docker Cloud Build Status](https://img.shields.io/docker/cloud/build/stefanfluit/ansible-aws-container)
![Docker Cloud Automated build](https://img.shields.io/docker/cloud/automated/stefanfluit/ansible-aws-container)

### What is installed? ###

* Latest Ansible version
* All required Python packages from requirements.txt
* All roles I use from requirements.yml
* Boto3 & Hetzner Hcloud Python SDKs

### How do I get set up? ###

* `docker pull stefanfluit/ansible-aws-container`

### Tags ###

* Only latest for now

### Who do I talk to? ###

* stefan@fluit-online.nl

### Example bitbucket-pipelines.yml file for Bitbucket automation ###

```
image: stefanfluit/ansible-aws-container:latest

pipelines:
  branches:
    main:
      - step:
          name: Validate Ansible files before running the pipeline
          script:
            - ansible-playbook ansible-playbook.yml -i inventory.yml --syntax-check

      - step:
          name: Run playbook
          script:
            - ansible-playbook ansible-playbook.yml -i inventory.yml
```

### Running a playbook: ###
```
docker run --rm -it \
  -v ${PWD}:/ansible stefanfluit/ansible-aws-container \
  ansible-playbook -i inventory.yml ansible-playbook.yml
```

### Or using a compose file: ###
```
version: "3.9"
services:
  ansible-container:
    restart: unless-stopped
    image: stefanfluit/ansible-aws-container:latest
    volumes:
      - .:/ansible
    command: 
      - "ansible-playbook -i /ansible/inventory.yml /ansible/ansible-playbook.yml"
```