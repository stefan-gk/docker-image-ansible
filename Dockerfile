FROM python:3.10.2-alpine3.15

LABEL maintainer="stefan@fluit-online.nl"

# Install some dependencies
RUN apk --no-cache --update add \
        ca-certificates \
        git \
        openssh-client \
        openssl \
        python3\
        py3-pip \
        py3-cryptography \
        rsync \
        yamllint

RUN apk --update add --virtual \
        python3-dev \
        libffi-dev \
        openssl-dev \
        build-base \
        curl

# Clear apk cache
RUN rm -rf /var/cache/apk/*

# Upgrade pip
RUN python3.10 -m pip install --upgrade pip

# Install Ansible using pip
RUN python3.10 -m pip install ansible

# Ensure all files are accessible
COPY . .

# Install Ansible roles
RUN ansible-galaxy install -r requirements.yml

# Install pip packages
RUN python3.10 -m pip install -r requirements.txt

# Ansible version
RUN ansible --version